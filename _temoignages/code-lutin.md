---
layout: temoignage
nom: Code Lutin
homepage: https://www.codelutin.com/
---

# Code Lutin

## Qui sommes-nous ?

Code Lutin a été créée en 2002. Son histoire est faite des échecs et des améliorations que nous avons accumulés au fil des années, alors que nous essayions de créer une entreprise dans laquelle il est possible de travailler sans hiérarchie.

Le logiciel libre, ses valeurs et ses principes sont à la genèse de Code Lutin. Dès l’origine, nous avons emprunté au projet Debian son mode de fonctionnement : discussion démocratique, élection avec la méthode Condorcet, transparence.

Nous publions l’intégralité des développements que nous réalisons en interne (bibliothèques, outils, logiciels) sous licence libre et sur notre [GitLab public](https://gitlab.nuiton.org/). Évidemment, aujourd’hui, à l’ère de GitHub et du règne de l’open-source, ça semble aller de soi… mais il y a 20 ans, quand il n’y avait que CVS, sourceforge et savannah, c’était précurseur d’avoir notre propre forge et de faire du Libre !

Nous avons cofondé [Alliance Libre](http://alliance-libre.org/), un regroupement d’entreprises nantaises qui évoluent dans l’écosystème open-source ainsi que le réseau informel [Libre-Entreprise](https://libre-entreprise.org/) dont la charte reflète nos valeurs. Au-delà du partage de nos pratiques, Libre-Entreprise regroupe autant d’entreprises partenaires avec lesquelles nous travaillons pour nous compléter mutuellement afin d’assurer une prestation globale. Le réseau regroupe plus de 150 personnes et les synergies sont régulières.

Nous pratiquons la démocratie directe : contrairement à beaucoup de structures qui se déclarent démocratiques ou coopératives et dont le seul vote consiste à désigner un chef, nous décidons collectivement de tout. Ce fonctionnement demande une organisation, des méthodes de délibération, des outils ([Pollen](https://pollen.cl)) et une culture démocratique (savoir exprimer son point de vue, le défendre, écouter, s’abstenir, résumer…).

En 2020, nous sommes une SAS à « Participation Ouvrière ». Cette forme juridique permet de donner à l’ensemble du personnel (réuni dans une « société coopérative de main-d’œuvre ») le contrôle de l’entreprise par l’attribution de voix à l’assemblée générale.

Cela ne nous dispense pas d’avoir des actionnaires, au sens traditionnel (c’est incontournable dans une société par actions). Cependant, nous tenons à ce que le capital soit partagé : tous les lutins sont invités à souscrire un nombre d’actions proportionnel à son temps de travail hebdomadaire dans la structure (50 parts pour un plein-temps, 25 pour un mi-temps, etc.). Cela nous a pris de longues années pour équilibrer le capital entre nous : en effet, nous avions manqué de vigilance sur ce point les premières années et le départ d’un fondateur aurait mis en péril la société. Nos statuts interdisent désormais tout actionnaire extérieur à l’entreprise.

En 2020, nous avons atteint un effectif de 20 :
— Une assistante de gestion
— Une commerciale
— Un technico-commercial
— Un AMOA
— Un concepteur UX
— Quinze assurant la réalisation des logiciels (développement Java, Javascript, architecture logicielle, modélisation, base de données, gestion de la construction et de l’intégration continue, spécification, documentation…)

Nous en sommes très heureux : c’est un équilibre parfait qui nous permet de rester démocratique, à échelle humaine, de nous diversifier dans divers projets simultanément tout en maintenant des équipes. C’était beaucoup plus difficile de faire fonctionner Code Lutin lorsque, les premières années, nous étions quelque-uns. Même pour une petite structure, il y a énormément de travail administratif à faire : nous étions à peine trois que la quatrième recrue était déjà une personne chargée de l’administratif à plein-temps (et c’est très cher de payer 4 personnes quand seules 3 sont facturées) !

Nous allons sûrement rester à ce chiffre afin de nous assurer que notre mode de fonctionnement est compatible avec une augmentation des effectifs (abandonner la démocratie directe pour la [démocratie liquide](https://fr.wikipedia.org/wiki/Démocratie_liquide) ?) ; sinon, nous essaimerons !

Enfin : le plus dur, dans cette aventure, est de trouver les clients. On peut être les meilleurs du monde, sans client, on ne sert à rien. Donc il est important de ne pas être que technique. Au-delà des démarches commerciales qui sont nécessaires, il faut réussir à donner confiance au client pour qu’il travaille avec nous plutôt qu’un autre. De ce point de vue là, nous sommes une entreprise traditionnelle : il faut s’adapter au marché, à la demande. Il faut accepter de travailler pour des clients qui ne partagent pas nos valeurs : une fois la confiance instaurée, nous pourrons essayer d'accompagner vers plus de Libre.

## Vie de tous les jours

Nous tenons une réunion hebdomadaire : seul moment de la semaine où la présence de tout le monde est attendue. Nous alternons, une semaine sur deux, entre la « réunion lutin » qui concerne la vie de la société et « réunion dév » lors de laquelle nous échangeons sur la technique pure.

Nous avons une liste de diffusion interne sur laquelle se passent la plupart des échanges. De plus en plus, nous utilisons Matrix et Element : la multiplicité des salons permet de travailler sur des sujets plus précis en réunissant l’équipe que le sujet intéresse : éco-conception, administration système, conception UX, commercial, mécénat, recrutement…

Nous utilisons régulièrement Pollen, outil que nous avons développé et redéveloppé maintes fois car c’est un outil indispensable à notre fonctionnement.

## Répartition des revenus

Code Lutin est réputée pour sa grande prudence. Étant donné que notre modèle économique ne nous assure pas de revenus récurrents, nous sommes exposés à la fluctuation de la demande et les résultats de l’entreprise peuvent fortement varier d’une année à l’autre, comme pour la plupart des ESN !

Aussi, nous avons choisi un mode de rémunération prudent : un salaire fixe, en dessous du marché, qu’on est certain de pouvoir maintenir même pendant les périodes de baisse d’activité. En complément, une rémunération variable qui correspond à une part des bénéfices réalisés.

Chez Code Lutin, nous estimons que c’est le temps de travail qui détermine l’effort fournit pour faire fonctionner Code Lutin. Ainsi, quelque-soit le poste, l’expérience, la qualification, le tarif journalier auquel on est facturé, le revenu dépend uniquement du temps de travail : une personne à mi-temps gagne deux fois moins qu’une personne à plein-temps.

À partir de ce cadre, nous sollicitons tous les moyens à notre disposition pour distribuer les bénéfices : primes, intéressement, participation, dividendes de travail (une spécificité de la « participation ouvrière ») et dividendes de capital.

Depuis quelques années, nous distribuons l’intégralité de nos bénéfices. En effet, pendant les premières années de Code Lutin, les bénéfices ont été accumulés dans les fonds propres : accumuler ce trésor de guerre nous a permis de ne jamais avoir été en difficulté de trésorerie. Aujourd’hui, ces fonds propres sont suffisants pour pouvoir encaisser plusieurs mois de totale inactivité pour Code Lutin : continuer à accumuler serait contre-productif puisque si cette situation se présente vraiment, nous aurons d’autres possibilités (chômage partiel, voire liquidation).

## Recrutement

Là aussi, notre prudence nous guide. Nous recrutons avec parcimonie, c’est-à-dire à peine plus d’une personne par an en moyenne.

Le recrutement est très difficile. L’expérience nous a appris qu’il faut écarter les personnes qui n’ont aucune sensibilité au Libre (c’est important de maintenir une cohésion), les personnes qui ne veulent ou ne peuvent adopter notre mode de fonctionnement horizontal et démocratique…

Enfin, notre mode de rémunération atypique est souvent une difficulté : après plusieurs années d’expérience et l’habitude d’un certain niveau de vie, rejoindre Code Lutin représente un gain en revenus mais souvent une perte de salaire fixe qui peut être difficile pour le foyer. Toutefois, si cela a ralenti certains recrutements, personne n’a dû renoncer à rejoindre Code Lutin pour cette raison. Prendre le risque de rejoindre Code Lutin, c’est aussi donner la preuve qu’on est prêt à un petit sacrifice, qu’on est bien dans une démarche d’entrepreneuriat et qu’on est prêt à vivre les bons moments comme les mauvais.
