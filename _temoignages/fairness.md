---
layout: temoignage
nom: Fairness
homepage: https://fairness.coop
---

## Qui sommes-nous ?

Une coopérative d'accompagnement dans la conception responsable de service numérique.
Nous sommes une équipe composée de développeurs et d'agilistes.
Nous travaillons de concert avec nos clients pour minimiser l'impact environnemental des services numériques que nous concevons tout en apportant notre expertise technique sur leur problématique.

### Structuration

Fairness est née de la transformation d'une SAS en Scop Arl. Le gérant historique et les salariés se sont donc rassemblés pour transformer leur entreprise en coopérative. La transformation a été assez simple car le fonctionnement précédent comportait un management et une hiérarchie très horizontale. Tout le monde avait déjà l'habitude de prendre les décisions de manière collégiale et cela nous semblait la suite logique que ce changement de structure juridique.

Nous nous sommes également faits accompagner par l'[URSCOP](https://www.les-scop.coop/sites/fr/), l'union régionale des scops, pour effectuer cette transition.

Vous pouvez retrouver plus d'informations sur notre transition dans notre article à ce sujet : <https://fairness.coop/blog/2020/transformation-en-cooperative/>

### Auto-organisation

Nous travaillons en auto-organisation, nous sommes nos propres chefs de projet, nos propres commerciaux, etc...

Chaque personne peut s'occuper de la relation commerciale et de chercher des clients.
L'équipe s'auto-organise, gère son temps, ses réunions, son client, sa facturation elle-même.
Elle est libre de choisir ses technologies et ses outils en fonction de ce qui répond le mieux aux besoins.

L'équipe se décompose ou recompose en fonction des projets et des clients. On peut travailler ensemble sur certains projets comme être avec nos clients sur d'autres.

### Coopération

Nous avons fait le choix d'une coopération obligatoire, qui se concrétise par l'entrée obligatoire en tant que coopérateur après deux ans de salariat dans l'entreprise et ce afin d'éviter de siloter les coopérateurs des salariés. Tout le monde fait partie des deux environnement à la fois.

Cela nous permet aussi de tous disposer d'une voix au chapitre lors de nos différentes assemblées.

Nous fonctionnons également avec un système de véto, qui permet à chaque personne d'ouvrir le débat sur une décision comme par exemple le choix d'un client. Si la personne estime que ce client n'est pas en accord avec nos valeurs, on décide ainsi de ne pas établir de relation. Nous expérimentons également le consentement systémique pour ce type de décision.

### Partage de connaissance

Nous nous retrouvons tous au travers de dojo. Ce sont des moments hors projets qui ont lieu minimum deux fois par mois, qui nous permettent d'échanger, de sortir du quotidien. Ces moments sont prompts aux discussions sur les échéances à venir dans la coopérative, sur les décisions à prendre, sur l'avancement de tel ou tel projet, mais ils permettent également de se retrouver pour tester de nouvelles choses.

Nous passons donc du temps à expérimenter de nouvelles technologies, mais aussi de nouvelles méthodologies, de nouveaux outils. C'est aussi un moment de retour d'expérience et de partage sur nos expériences en mission.

Ces moments permettent de ne pas se sentir déconnecté de la coopérative quand certains d'entre nous partent quelque temps en mission.
Si vous voulez en savoir plus sur ce mode de fonctionnement, vous pouvez lire notre article à ce sujet <https://fairness.coop/blog/2019/remue-meninges-collectif/>

## Vie de tous les jours

Nous avons quelques places dans des coworking (Paris et Nantes) qui nous permettent de nous retrouver quand nous ne sommes pas en mission ou quand nos missions peuvent se dérouler à distance.

Ce "camp de base" nous permet d'avoir une accroche physique sans qu'elle ne pèse financièrement sur l'entreprise. Il permet aussi de se retrouver pour nos dojos.

A cela, nos outils du quotidien pour communiquer sont les classiques Email, Slack, Gitlab en fonction du sujet.

## Salaire

Chaque membre de la coopérative est au courant des salaires de chacun. Il.elle est aussi au courant de toutes les prévisions financières de l'entreprise. Ce qui permet à chacun de voir ce qu'il.elle ramène à la coopérative (même si ce terme est un peu galvaudé car l'apport n'est pas uniquement financier).

Nous discutons donc des salaires ensemble, chacun peut demander le salaire qu'il.elle souhaite et argumente sur pourquoi il.elle estime en avoir besoin. Si tout le monde est d'accord alors ce salaire est accepté. Il en va de même pour les augmentations.

Comme chaque personne a une visibilité sur les entrées et sorties, chacun peut se faire une idée du "coût" de son salaire pour l'entreprise.

## Recrutement

Nous n'avons pas de règle particulière pour le recrutement à part peut être le fait que la personne soit prête à s'impliquer dans la coopérative.

Nous rencontrons les candidat.e.s en équipe. Le contact humain et l'esprit d'équipe sont des choses importantes pour nous. La majorité de nos contrats sont des CDIs même si nous sommes ouverts aussi aux stages et aux alternances. Nous profitions généralement des dojos pour nous rencontrer.

A cela s'ajoute le fait que nous n'avons pas de restrictions sur le temps de travail souhaité. Si une personne souhaite être à temps partiel, nous n'y voyons pas d'inconvénient.

